# Faker Data

Data for creating fake data. This data is exposed as a C library which can be used
directly by multiple programming languages (can use FFI/interop instead of parsing).
The data files used to generate the C library are available for parsing if desired.

All data is stored inside of CSV files. If a column has an array of values (e.g.
credit card start sequences), then those values are separated with a pipe (`|`).
If a column is meant to be binary, then it will be a series of two digit hex values
(e.g. `0F`). If it is a sequence of binary, then they will be separated by a space
(e.g. `48 00 65 00 22 00`). The value `-` represents "No Data" unless stated
otherwise. Ranges are stored with the `..` operator - which is inclusive (e.g.
`1..4` is `[1, 2, 3, 4]`).

This project is licensed under the Apache-2 license.

## Compilation requirements

To compile the project, you will need a compiler that supports C99 and
C++17. While the header and tests are written in C99, the actual
implementation is written in C++17. By using C++17, we can use template
inference for std:array as well as constexpr expressions to compile our
data sets into the library binary.

## C representation

For C code, we use `const char*` for our string values. These values
live in the executable file and do not require dynamic allocation (or
deallocation). We use null terminated and utf-8 encoded strings when
possible. When not possible, we will include a string length (this is
most common for edge cases where we have utf-16 bytes in a
`const char*` to mimic encoding mismatches for incoming data).

For text with multiple possible subsets of data (e.g. English, French,
Chinese), we separate the data by the complexity to process rather than
by locale. These complexities are divided into `Rudimentary`, `Basic`,
`Intermediate`, `Advanced`, and `Complex`. The groupings are based on
my experience in the US software market for how common a data set is to
appear in real-life situations, as well as how well software can handle
those data sets. Each step where software begins to break down represents
another division in the group.

For instance, for names we will have ASCII alphabetic-only names are
Rudimentary, ASCII hyphenated names and with spaces are Basic, any
printable ASCII names are Intermediate, Latin-based Unicode names are
Advanced (i.e. Unicode names not needing UTF-16 surrogate pairs), and Unicode
names in non-latin scripts (cryllic, arabic, chinese, etc) are in
complex (i.e. Unicode names needing UTF-16 surrogate pairs).

Arrays of data will have a size provided (e.g. an array of card number
lengths). Additionally, methods to get a particular data set have an
out parameter for the size of the data set.

Only raw C-arrays are exposed. We do not try to expose any sort of
hash-map, property lookup, filtering, etc. The goal of this library
is to be the basis for software which does the data format optimizations
or which does code generation. As such, we leave it up to the user of
the library to build their ideal data format (and most likely generate
the code needed for any pre-computations to optimize for their use case).

## Data Versioning

The data provided is versioned. These versions include the version the
data was introduced, as well as the version it was removed (if applicable).
No data is permanently deleted from the data set. Changes are done by
"removing" the old entry and "adding" a new entry.

This allows code to "lock" itself to a particular version of the data
while still allowing updates for bug fixes, performance improvements,
etc. It also allows developers to separate updating the library from
updating the data in the library into two steps. This allows for testing
if a bug was introduced because of a code change inside our library (e.g.
we introduced or fixed a bug), or if a bug was introduced by a change to
the data set (e.g. we added a new Unicode example which uses characters
which an application's font doesn't support).

The FakerDataVersionNumber enum will define which data versions are available
for a given edition of the library. It is recommended that applications
which need consistent data formats should filter out any data entries
introduced outside their data format.
