#include <tau/tau.h>
#include <faker-data/faker-data.h>

TAU_MAIN()

TEST(World, Countries) {
    size_t numCountries = 0;
    const FakerDataCountry* countries = faker_data_countries(&numCountries);
    CHECK_GE(numCountries, 278);

    size_t v0Count = 0;
    for (size_t i = 0; i < numCountries; ++i) {
        const FakerDataCountry* country = &countries[i];
        CHECK_NE(country->name, NULL);
        CHECK_NE(country->genc, NULL);
        CHECK_NE(country->isoAlpha2, NULL);
        CHECK_NE(country->isoAlpha3, NULL);
        CHECK_NE(country->isoNumericCode, NULL);
        CHECK_NE(country->stanag, NULL);
        CHECK_NE(country->domain, NULL);
        CHECK_EQ(country->versionInfo.added, FAKER_DATA_VERSION_0);
        if (faker_data_is_active(0, &country->versionInfo)) {
            CHECK_EQ(country->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }
    CHECK_EQ(v0Count, 278);
}

TEST(World, Timezones) {
    size_t num = 0;
    const FakerDataStringEntry * values = faker_data_timezones(&num);
    CHECK_GE(num, 418);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        CHECK_NE(strlen(val->string), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 418);
}

TEST(Person, GivenNames) {
    size_t num = 0;
    const FakerDataPersonNamePieceEntry * values = faker_data_given_names(&num);
    CHECK_GE(num, 477);

    size_t v0Count = 0;
    for (size_t i = 0; i < num; ++i) {
        const FakerDataPersonNamePieceEntry* val = &values[i];
        CHECK_NE(val->name, NULL);
        CHECK_NE(strlen(val->name), 0);
        CHECK_GE(val->sex, FAKER_DATA_PERSON_SEX_MALE);
        CHECK_LE(val->sex, FAKER_DATA_PERSON_SEX_FEMALE);
        CHECK_GE(val->complexity, FAKER_DATA_COMPLEXITY_RUDIMENTARY);
        CHECK_LE(val->complexity, FAKER_DATA_COMPLEXITY_COMPLEX);

        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }
    CHECK_EQ(v0Count, 477);
}

TEST(Person, Prefixes) {
    size_t num = 0;
    const FakerDataPersonNamePieceEntry * values = faker_data_prefixes(&num);
    CHECK_GE(num, 34);

    size_t v0Count = 0;
    for (size_t i = 0; i < num; ++i) {
        const FakerDataPersonNamePieceEntry* val = &values[i];
        CHECK_NE(val->name, NULL);
        CHECK_NE(strlen(val->name), 0);
        CHECK_GE(val->sex, FAKER_DATA_PERSON_SEX_MALE);
        CHECK_LE(val->sex, FAKER_DATA_PERSON_SEX_FEMALE);
        CHECK_GE(val->complexity, FAKER_DATA_COMPLEXITY_RUDIMENTARY);
        CHECK_LE(val->complexity, FAKER_DATA_COMPLEXITY_COMPLEX);

        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }
    CHECK_EQ(v0Count, 34);
}

TEST(Person, SurNames) {
    size_t num = 0;
    const FakerDataTextEntry * values = faker_data_surnames(&num);
    CHECK_GE(num, 169);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataTextEntry* val = &values[i];
        CHECK_NE(val->text, NULL);
        CHECK_NE(strlen(val->text), 0);
        CHECK_GE(val->complexity, FAKER_DATA_COMPLEXITY_RUDIMENTARY);
        CHECK_LE(val->complexity, FAKER_DATA_COMPLEXITY_COMPLEX);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 169);
}

TEST(Person, Suffixes) {
    size_t num = 0;
    const FakerDataTextEntry * values = faker_data_suffixes(&num);
    CHECK_GE(num, 15);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataTextEntry* val = &values[i];
        CHECK_NE(val->text, NULL);
        CHECK_NE(strlen(val->text), 0);
        CHECK_GE(val->complexity, FAKER_DATA_COMPLEXITY_RUDIMENTARY);
        CHECK_LE(val->complexity, FAKER_DATA_COMPLEXITY_COMPLEX);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 15);
}

TEST(Malicious, EdgeCases) {
    size_t num = 0;
    const FakerDataLengthStringEntry * values = faker_data_edge_cases(&num);
    CHECK_GE(num, 9);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataLengthStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 9);
}

TEST(Malicious, Xss) {
    size_t num = 0;
    const FakerDataStringEntry * values = faker_data_xss(&num);
    CHECK_GE(num, 22);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        CHECK_NE(strlen(val->string), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 22);
}

TEST(Malicious, SqlInject) {
    size_t num = 0;
    const FakerDataStringEntry * values = faker_data_sql_injection(&num);
    CHECK_GE(num, 27);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        CHECK_NE(strlen(val->string), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 27);
}

TEST(Malicious, FormatInject) {
    size_t num = 0;
    const FakerDataStringEntry * values = faker_data_format_injection(&num);
    CHECK_GE(num, 20);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        CHECK_NE(strlen(val->string), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 20);
}

TEST(Financial, Currencies) {
    size_t num = 0;
    const FakerDataCurrency * values = faker_data_currencies(&num);
    CHECK_GE(num, 181);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataCurrency* val = &values[i];
        CHECK_NE(val->name, NULL);
        CHECK_NE(strlen(val->name), 0);
        CHECK_NE(val->isoCode, NULL);
        CHECK_EQ(strlen(val->isoCode), 3);
        CHECK_NE(val->symbol, NULL);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 181);
}

TEST(Financial, AccountNames) {
    size_t num = 0;
    const FakerDataTextEntry * values = faker_data_account_names(&num);
    CHECK_GE(num, 45);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataTextEntry* val = &values[i];
        CHECK_NE(val->text, NULL);
        CHECK_NE(strlen(val->text), 0);
        CHECK_GE(val->complexity, FAKER_DATA_COMPLEXITY_RUDIMENTARY);
        CHECK_LE(val->complexity, FAKER_DATA_COMPLEXITY_COMPLEX);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 45);
}

TEST(Financial, TransactionTypes) {
    size_t num = 0;
    const FakerDataStringEntry * values = faker_data_transaction_types(&num);
    CHECK_GE(num, 4);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        CHECK_NE(strlen(val->string), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 4);
}


TEST(Financial, CreditCards) {
    size_t num = 0;
    const FakerDataCreditCardNetwork * values = faker_data_cc_networks(&num);
    CHECK_GE(num, 7);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataCreditCardNetwork* val = &values[i];
        CHECK_NE(val->id, NULL);
        CHECK_NE(strlen(val->id), 0);
        CHECK_NE(val->name, NULL);
        CHECK_NE(strlen(val->name), 0);
        CHECK_GT(val->numCardStarts, 0);
        CHECK_NE(val->cardStarts, NULL);
        CHECK_GT(val->numCardLens, 0);
        CHECK_NE(val->cardLens, NULL);
        CHECK_GT(val->cvvLen, 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 7);
}

TEST(Utils, IsActive) {
    FakerDataVersionNumber curVersion = 0;
    FakerDataVersionNumber nextVersion = 1;

    FakerDataVersionInfo added0NotRemoved;
    added0NotRemoved.removeInfo.wasRemoved = 0;
    added0NotRemoved.added = 0;

    FakerDataVersionInfo added0RemovedV1;
    added0RemovedV1.removeInfo.wasRemoved = 1;
    added0RemovedV1.removeInfo.removedIn = 1;
    added0RemovedV1.added = 0;

    FakerDataVersionInfo added0RemovedV2;
    added0RemovedV2.removeInfo.wasRemoved = 1;
    added0RemovedV2.removeInfo.removedIn = 2;
    added0RemovedV2.added = 0;

    FakerDataVersionInfo added1NotRemoved;
    added1NotRemoved.removeInfo.wasRemoved = 0;
    added1NotRemoved.added = 1;

    // currently introduced
    CHECK(faker_data_is_active(curVersion, &added0NotRemoved));
    CHECK(faker_data_is_active(nextVersion, &added1NotRemoved));
    // not removed yet
    CHECK(faker_data_is_active(curVersion, &added0RemovedV1));
    // previously added
    CHECK(faker_data_is_active(nextVersion, &added0NotRemoved));
    // previously added, not yet removed
    CHECK(faker_data_is_active(nextVersion, &added0RemovedV2));

    // not yet introduced
    CHECK_FALSE(faker_data_is_active(curVersion, &added1NotRemoved));
    // currently removed
    CHECK_FALSE(faker_data_is_active(nextVersion, &added0RemovedV1));
    // currently removed
    CHECK_FALSE(faker_data_is_active(2, &added0RemovedV2));
    // previously removed
    CHECK_FALSE(faker_data_is_active(2, &added0RemovedV1));
}

TEST(Internet, UrlProtocols) {
    size_t num = 0;
    const FakerDataStringEntry * values = faker_data_url_protocols(&num);
    CHECK_GE(num, 5);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        CHECK_NE(strlen(val->string), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 5);
}

TEST(Internet, TopLevelDomains) {
    size_t num = 0;
    const FakerDataStringEntry * values = faker_data_top_level_domains(&num);
    CHECK_GE(num, 140);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        CHECK_NE(strlen(val->string), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 140);
}

TEST(Aviation, AircraftTypes) {
    size_t num = 0;
    const FakerDataStringEntry * values = faker_data_aircraft_types(&num);
    CHECK_GE(num, 3);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        CHECK_NE(strlen(val->string), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 3);
}

TEST(Aviation, Airlines) {
    size_t num = 0;
    const FakerDataAviationEntry * values = faker_data_airlines(&num);
    CHECK_GE(num, 86);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataAviationEntry* val = &values[i];
        CHECK_NE(val->name, NULL);
        CHECK_NE(strlen(val->name), 0);
        CHECK_NE(val->iata, NULL);
        CHECK_NE(strlen(val->iata), 0);
        CHECK_NE(val->icao, NULL);
        CHECK_NE(strlen(val->icao), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 86);
}

TEST(Aviation, Airplanes) {
    size_t num = 0;
    const FakerDataAviationEntry * values = faker_data_airplanes(&num);
    CHECK_GE(num, 148);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataAviationEntry* val = &values[i];
        CHECK_NE(val->name, NULL);
        CHECK_NE(strlen(val->name), 0);
        CHECK_NE(val->iata, NULL);
        CHECK_NE(strlen(val->iata), 0);
        CHECK_NE(val->icao, NULL);
        CHECK_NE(strlen(val->icao), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 148);
}

TEST(Aviation, Airports) {
    size_t num = 0;
    const FakerDataAviationEntry * values = faker_data_airports(&num);
    CHECK_GE(num, 2819);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataAviationEntry* val = &values[i];
        CHECK_NE(val->name, NULL);
        CHECK_NE(strlen(val->name), 0);
        CHECK_NE(val->iata, NULL);
        CHECK_NE(strlen(val->iata), 0);
        CHECK_NE(val->icao, NULL);
        CHECK_NE(strlen(val->icao), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 2819);
}

TEST(Colors, Colors) {
    size_t num = 0;
    const FakerDataColorEntry * values = faker_data_colors(&num);
    CHECK_GE(num, 163);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataColorEntry* val = &values[i];
        CHECK_NE(val->name, NULL);
        CHECK_NE(strlen(val->name), 0);
        CHECK_NE(val->hex, NULL);
        CHECK_EQ(strlen(val->hex), 6);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 163);
}

TEST(Computers, MimeTypes) {
    size_t num = 0;
    const FakerDataStringEntry * values = faker_data_mime_types(&num);
    CHECK_GE(num, 2053);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        CHECK_NE(strlen(val->string), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 2053);
}

TEST(Computers, FileExtensions) {
    size_t num = 0;
    const FakerDataStringEntry * values = faker_data_file_extensions(&num);
    CHECK_GE(num, 706);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        CHECK_NE(strlen(val->string), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 706);
}

TEST(Computers, FileTypes) {
    size_t num = 0;
    const FakerDataStringEntry * values = faker_data_file_types(&num);
    CHECK_GE(num, 8);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        CHECK_NE(strlen(val->string), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 8);
}

TEST(Computers, FileMimeMapping) {
    size_t num = 0;
    const FakerDataFileMimeMappingEntry * values = faker_data_file_mime_mapping(&num);
    CHECK_GE(num, 145);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataFileMimeMappingEntry* val = &values[i];
        CHECK_NE(val->mimeType, NULL);
        CHECK_NE(strlen(val->mimeType), 0);
        CHECK_NE(val->extension, NULL);
        CHECK_NE(strlen(val->extension), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 145);
}

TEST(Computers, CMakeSystemNames) {
    size_t num = 0;
    const FakerDataStringEntry * values = faker_data_cmake_system_names(&num);
    CHECK_GE(num, 3);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        CHECK_NE(strlen(val->string), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 3);
}

TEST(Computers, Os) {
    size_t num = 0;
    const FakerDataStringEntry * values = faker_data_os(&num);
    CHECK_GE(num, 26);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        CHECK_NE(strlen(val->string), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 26);
}

TEST(Computers, CpuArch) {
    size_t num = 0;
    const FakerDataStringEntry * values = faker_data_cpu_architectures(&num);
    CHECK_GE(num, 61);

    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataStringEntry* val = &values[i];
        CHECK_NE(val->string, NULL);
        CHECK_NE(strlen(val->string), 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 61);
}

TEST(Text, ASCII) {
    size_t num;
    const FakerDataCharSetGroup* values = faker_data_ascii_character_set_groups(&num);
    CHECK_GE(num, 27);


    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataCharSetGroup* val = &values[i];
        CHECK_NE(val->name, NULL);
        CHECK_NE(strlen(val->name), 0);
        CHECK_NE(val->description, NULL);
        CHECK_NE(strlen(val->description), 0);
        CHECK_GT(val->numRanges, 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 27);
}

TEST(Text, ExtendedASCII) {
    size_t num;
    const FakerDataCharSetGroup* values = faker_data_extended_ascii_character_set_groups(&num);
    CHECK_GE(num, 28);


    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataCharSetGroup* val = &values[i];
        CHECK_NE(val->name, NULL);
        CHECK_NE(strlen(val->name), 0);
        CHECK_NE(val->description, NULL);
        CHECK_NE(strlen(val->description), 0);
        CHECK_GT(val->numRanges, 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 28);
}

TEST(Text, UnicodeRange) {
    size_t num;
    const FakerDataUnicodeCategory* values = faker_data_unicode_character_categories(&num);
    CHECK_GE(num, 29);


    size_t v0Count = 0;

    for (size_t i = 0; i < num; ++i) {
        const FakerDataUnicodeCategory* val = &values[i];
        CHECK_GE(val->category, UNICODE_CATEGORY_CC);
        CHECK_LE(val->category, UNICODE_CATEGORY_ZS);
        CHECK_GT(val->numRanges, 0);
        if (faker_data_is_active(0, &val->versionInfo)) {
            CHECK_EQ(val->versionInfo.removeInfo.wasRemoved, 0);
            ++v0Count;
        }
        else {
            CHECK_FALSE(1);
        }
    }

    CHECK_EQ(v0Count, 29);
}
