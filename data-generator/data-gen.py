import csv
import sys
import os
import string

########################################################
# The utility methods
########################################################

versionColumn = 'VERSION'
removedColumn='REMOVED'

def c_escape(str=bytes()):
    mp = ""
    for c in str:
        if c == ord('\\'):
            mp += "\\\\"
        elif c == ord('?'):
            mp += "\\?"
        elif c == ord('\''):
            mp += "\\'"
        elif c == ord('"'):
            mp += "\\\""
        elif c == ord('\a'):
            mp += "\\a"
        elif c == ord('\b'):
            mp += "\\b"
        elif c == ord('\f'):
            mp += "\\f"
        elif c == ord('\n'):
            mp += "\\n"
        elif c == ord('\r'):
            mp += "\\r"
        elif c == ord('\t'):
            mp += "\\t"
        elif c == ord('\v'):
            mp += "\\v"
        elif chr(c) in string.printable:
            mp += chr(c)
        else:
            mp += "\\x" + format(c, '02x') + "\" \""
    return mp


def c_str(value=''):
    if value.strip() == '-':
        return "\"\""
    else:
        return '"' + c_escape(value.encode('utf8')) + '"'


def c_version(value=''):
    if value.strip() == '0':
        return "FakerDataVersionNumber::FAKER_DATA_VERSION_0"
    else:
        raise Exception("Unknown version {}".format(value))


def c_removed_version(value=''):
    if value == None or value.strip() == '-':
        return "FAKER_DATA_VERSION_0"
    else:
        return c_version(value)


def c_was_removed(value=''):
    if value == None or value.strip() == '-':
        return "0"
    else:
        return "1"


def c_complexity(value=''):
    if value.strip().upper() == 'RUDIMENTARY':
        return "FakerDataComplexity::FAKER_DATA_COMPLEXITY_RUDIMENTARY"
    elif value.strip().upper() == 'BASIC':
        return "FakerDataComplexity::FAKER_DATA_COMPLEXITY_BASIC"
    elif value.strip().upper() == 'INTERMEDIATE':
        return "FakerDataComplexity::FAKER_DATA_COMPLEXITY_INTERMEDIATE"
    elif value.strip().upper() == 'ADVANCED':
        return "FakerDataComplexity::FAKER_DATA_COMPLEXITY_ADVANCED"
    elif value.strip().upper() == 'COMPLEX':
        return "FakerDataComplexity::FAKER_DATA_COMPLEXITY_COMPLEX"
    else:
        raise Exception("Unknown complexity {}".format(value))


def c_sex(value=''):
    if value.strip().upper() == 'MALE':
        return "FakerDataPersonSex::FAKER_DATA_PERSON_SEX_MALE"
    elif value.strip().upper() == 'FEMALE':
        return "FakerDataPersonSex::FAKER_DATA_PERSON_SEX_FEMALE"
    else:
        raise Exception("Unknown sex {}".format(value))


def write_string_entries(csvfile, outfile, methodSuffix, valueColumn):
    outfile.write("\nstatic constexpr auto {method} = std::array{{".format(method=methodSuffix))
    reader = csv.DictReader(csvfile)
    for row in reader:
        outfile.write(
            """
FakerDataStringEntry{{
    .string={text},
    .versionInfo={{
        .added={version},
        .removeInfo={{
            .wasRemoved={wasRemoved},
            .removedIn={removed}
        }}
    }}
}},""".format(
                text=c_str(row[valueColumn]),
                version=c_version(row[versionColumn]),
                removed=c_removed_version(row[removedColumn]),
                wasRemoved=c_was_removed(row[removedColumn])
            )
        )
    outfile.write("""
}};

const FakerDataStringEntry *faker_data_{method}(size_t *outNumEntries) {{
    if (outNumEntries != 0) {{
        *outNumEntries = {method}.size();
    }}
    return {method}.data();
}}
""".format(method=methodSuffix))


def write_character_groups(csvfile, outfile, methodSuffix, nameColumn, valueColumn, descriptionColumn):
    reader = csv.DictReader(csvfile)

    rows = []
    for row in reader:
        rows.append(row)
        values = [x.split('..') for x in row[valueColumn].split('|')]

        outfile.write("\nstatic constexpr auto {method}_{name} = std::array{{\n".format(
            method=methodSuffix,
            name=row[nameColumn],
        ))

        for range in values:
            if len(range) == 1:
                range = [range[0], range[0]]
            outfile.write("\tFakerDataCharRange{{.start=0x{start}, .end=0x{end}}},\n".format(
                start=range[0],
                end=range[1]
            ))

        outfile.write("};\n")

    outfile.write("\nstatic constexpr auto {method} = std::array{{".format(method=methodSuffix))

    for row in rows:
        outfile.write(
            """
    FakerDataCharSetGroup{{
        .numRanges={method}_{name}.size(),
        .ranges={method}_{name}.data(),
        .name={groupName},
        .description={groupDescription},
        .versionInfo={{
            .added={version},
            .removeInfo={{
                .wasRemoved={wasRemoved},
                .removedIn={removed}
            }}
        }}
    }},""".format(
                method=methodSuffix,
                name=row[nameColumn],
                groupName=c_str(row[nameColumn]),
                groupDescription=c_str(row[descriptionColumn]),
                version=c_version(row[versionColumn]),
                removed=c_removed_version(row[removedColumn]),
                wasRemoved=c_was_removed(row[removedColumn])
            )
        )
    outfile.write("""
}};

const FakerDataCharSetGroup *faker_data_{method}(size_t *outNumEntries) {{
    if (outNumEntries != 0) {{
        *outNumEntries = {method}.size();
    }}
    return {method}.data();
}}
""".format(method=methodSuffix))


def write_text_entries(csvfile, outfile, methodSuffix, valueColumn, complexityColumn = 'COMPLEXITY'):
    outfile.write("\nstatic constexpr auto {method} = std::array{{".format(method=methodSuffix))
    reader = csv.DictReader(csvfile)
    for row in reader:
        outfile.write(
            """
FakerDataTextEntry{{
    .text={name},
    .complexity={complexity},
    .versionInfo={{
        .added={version},
        .removeInfo={{
            .wasRemoved={wasRemoved},
            .removedIn={removed}
        }}
    }}
}},""".format(
                name=c_str(row[valueColumn]),
                version=c_version(row[versionColumn]),
                complexity=c_complexity(row[complexityColumn]),
                removed=c_removed_version(row[removedColumn]),
                wasRemoved=c_was_removed(row[removedColumn])
            )
        )
    outfile.write("""
}};

const FakerDataTextEntry *faker_data_{method}(size_t *outNumEntries) {{
    if (outNumEntries != 0) {{
        *outNumEntries = {method}.size();
    }}
    return {method}.data();
}}
""".format(method=methodSuffix))


def write_person_piece(csvfile, outfile, methodSuffix, valueColumn, complexityColumn = 'COMPLEXITY', sexColumn = 'SEX'):
    outfile.write("\nstatic constexpr auto {method} = std::array{{".format(method=methodSuffix))
    reader = csv.DictReader(csvfile)
    for row in reader:
        outfile.write(
            """
FakerDataPersonNamePieceEntry{{
    .name={name},
    .complexity={complexity},
    .sex={sex},
    .versionInfo={{
        .added={version},
        .removeInfo={{
            .wasRemoved={wasRemoved},
            .removedIn={removed}
        }}
    }}
}},""".format(
                name=c_str(row[valueColumn]),
                version=c_version(row[versionColumn]),
                complexity=c_complexity(row[complexityColumn]),
                sex=c_sex(row[sexColumn]),
                removed=c_removed_version(row[removedColumn]),
                wasRemoved=c_was_removed(row[removedColumn])
            )
        )
    outfile.write("""
}};

const FakerDataPersonNamePieceEntry *faker_data_{method}(size_t *outNumEntries) {{
    if (outNumEntries != 0) {{
        *outNumEntries = {method}.size();
    }}
    return {method}.data();
}}
""".format(method=methodSuffix))

def write_aviation_entries(csvfile, outfile, methodSuffix, valueColumn='Name', iataColumn='IATA', icaoColumn='ICAO'):
    outfile.write("\nstatic constexpr auto {method} = std::array{{".format(method=methodSuffix))
    reader = csv.DictReader(csvfile)
    for row in reader:
        outfile.write(
            """
FakerDataAviationEntry{{
    .name={name},
    .iata={iata},
    .icao={icao},
    .versionInfo={{
        .added={version},
        .removeInfo={{
            .wasRemoved={wasRemoved},
            .removedIn={removed}
        }}
    }}
}},""".format(
                name=c_str(row[valueColumn]),
                iata=c_str(row[iataColumn]),
                icao=c_str(row[icaoColumn]),
                version=c_version(row[versionColumn]),
                removed=c_removed_version(row[removedColumn]),
                wasRemoved=c_was_removed(row[removedColumn])
            )
        )
    outfile.write("""
}};

const FakerDataAviationEntry *faker_data_{method}(size_t *outNumEntries) {{
    if (outNumEntries != 0) {{
        *outNumEntries = {method}.size();
    }}
    return {method}.data();
}}
""".format(method=methodSuffix))





########################################################
# The methods for writing files
########################################################

def write_countries(outfile):
    outfile.write("\nstatic constexpr auto countries = std::array{")
    with(open(os.path.join('world', 'countries.csv'))) as countries_file:
        countries_reader = csv.DictReader(countries_file)
        for row in countries_reader:
            outfile.write(
                """
    FakerDataCountry{{
        .name={name},
        .genc={genc},
        .isoAlpha2={alpha2},
        .isoAlpha3={alpha3},
        .isoNumericCode={num},
        .stanag={stanag},
        .domain={domain},
        .versionInfo={{
            .added={version},
            .removeInfo={{
                .wasRemoved={wasRemoved},
                .removedIn={removed}
            }}
        }}
    }},""".format(
                    name=c_str(row['Name']),
                    genc=c_str(row['GENC']),
                    alpha2=c_str(row['ISO Alpha 2']),
                    alpha3=c_str(row['ISO Alpha 3']),
                    num=c_str(row['ISO Numeric']),
                    stanag=c_str(row['Stanag']),
                    domain=c_str(row['Internet']),
                    version=c_version(row[versionColumn]),
                    removed=c_removed_version(row[removedColumn]),
                    wasRemoved=c_was_removed(row[removedColumn])
                )
            )
    outfile.write("""
};

const FakerDataCountry *faker_data_countries(size_t *outNumEntries) {
    if (outNumEntries != 0) {
        *outNumEntries = countries.size();
    }
    return countries.data();
}
""")


def write_given_names(outfile):
    with(open(os.path.join('person', 'given_names.csv'))) as csvfile:
        write_person_piece(csvfile, outfile, 'given_names', 'NAME')


def write_prefixes(outfile):
    with(open(os.path.join('person', 'prefixes.csv'))) as csvfile:
        write_person_piece(csvfile, outfile, 'prefixes', 'NAME')


def write_surnames(outfile):
    with(open(os.path.join('person', 'surnames.csv'))) as csvfile:
        write_text_entries(csvfile, outfile, 'surnames', 'NAME')


def write_suffixes(outfile):
    with(open(os.path.join('person', 'suffixes.csv'))) as csvfile:
        write_text_entries(csvfile, outfile, 'suffixes', 'NAME')


def write_edge_cases(outfile):
    outfile.write("\nstatic constexpr auto edge_cases = std::array{")
    with(open(os.path.join("malicious", "edge_cases.csv"))) as file:
        data = csv.DictReader(file)
        for row in data:
            text = ''.join([chr(c) for c in bytes.fromhex(row['bytes'])])
            outfile.write(
                """
    FakerDataLengthStringEntry{{
        .len={len},
        .string={text},
        .versionInfo={{
            .added={version},
            .removeInfo={{
                .wasRemoved={wasRemoved},
                .removedIn={removed}
            }}
        }}
    }},""".format(
                    text=c_str(text),
                    version=c_version(row[versionColumn]),
                    removed=c_removed_version(row[removedColumn]),
                    wasRemoved=c_was_removed(row[removedColumn]),
                    len=len(text)
                )
            )
    outfile.write("""
};

const FakerDataLengthStringEntry *faker_data_edge_cases(size_t *outNumEntries) {
    if (outNumEntries != 0) {
        *outNumEntries = edge_cases.size();
    }
    return edge_cases.data();
}
""")


def write_xss(outfile):
    with(open(os.path.join('malicious', 'xss.csv'))) as csvfile:
        write_string_entries(csvfile, outfile, "xss", "ATTACK")


def write_sql_injection(outfile):
    with(open(os.path.join('malicious', 'sql_injection.csv'))) as csvfile:
        write_string_entries(csvfile, outfile, "sql_injection", "ATTACK")


def write_format_injection(outfile):
    with(open(os.path.join('malicious', 'format_injection.csv'))) as csvfile:
        write_string_entries(csvfile, outfile, "format_injection", "ATTACK")


def write_currencies(outfile):
    outfile.write("\nstatic constexpr auto currencies = std::array{")
    with(open(os.path.join('financial', 'currencies.csv'))) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            outfile.write(
                """
    FakerDataCurrency{{
        .name={name},
        .isoCode={isoCode},
        .symbol={symbol},
        .versionInfo={{
            .added={version},
            .removeInfo={{
                .wasRemoved={wasRemoved},
                .removedIn={removed}
            }}
        }}
    }},""".format(
                    name=c_str(row['Name']),
                    isoCode=c_str(row['ISO Code']),
                    symbol=c_str(row['Symbol']),
                    version=c_version(row[versionColumn]),
                    removed=c_removed_version(row[removedColumn]),
                    wasRemoved=c_was_removed(row[removedColumn])
                )
            )
    outfile.write("""
};

const FakerDataCurrency *faker_data_currencies(size_t *outNumEntries) {
    if (outNumEntries != 0) {
        *outNumEntries = currencies.size();
    }
    return currencies.data();
}
""")


def write_account_names(outfile):
    with(open(os.path.join('financial', 'account_names.csv'))) as csvfile:
        write_text_entries(csvfile, outfile, 'account_names', 'NAME')


def write_transaction_types(outfile):
    with(open(os.path.join('financial', 'transaction_types.csv'))) as csvfile:
        write_string_entries(csvfile, outfile, "transaction_types", "TYPE")


def write_cc_networks(outfile):
    ccNetworkCardStarts = []
    ccNetworkCardLens = []
    ccNetworkDefs = []

    with(open(os.path.join('financial', 'cc_networks.csv'))) as csvfile:
        data = csv.DictReader(csvfile)
        for row in data:
            network = row['id']
            starts = [c_str(t.strip()) for t in row['starts'].split('|')]
            lengths = [t.strip() for t in row['lengths'].split('|')]
            ccNetworkCardStarts.append("""
static constexpr auto cc_network_{network}_starts = std::array{{{starts}}};""".format(
                network=network,
                starts=','.join(starts),
            ))
            ccNetworkCardLens.append("""
static constexpr auto cc_network_{network}_lengths = std::array{{{lengths}}};""".format(
                network=network,
                lengths=','.join(["static_cast<size_t>(" + str(s) + ")" for s in lengths]),
            ))
            ccNetworkDefs.append("""
    FakerDataCreditCardNetwork{{
        .id={id},
        .name={name},
        .numCardStarts=cc_network_{network}_starts.size(),
        .cardStarts=cc_network_{network}_starts.data(),
        .numCardLens=cc_network_{network}_lengths.size(),
        .cardLens=cc_network_{network}_lengths.data(),
        .cvvLen={cvvLen},
        .versionInfo={{
            .added={version},
            .removeInfo={{
                .wasRemoved={wasRemoved},
                .removedIn={removed}
            }}
        }}
    }},
""".format(
                id=c_str(network),
                name=c_str(row['name']),
                network=network,
                cvvLen=str(row['cvvLen']),
                version=c_version(str(row[versionColumn])),
                removed=c_removed_version(row[removedColumn]),
                wasRemoved=c_was_removed(row[removedColumn])
            ))
    outfile.write(''.join(ccNetworkCardStarts))
    outfile.write(''.join(ccNetworkCardLens))
    outfile.write("\nstatic constexpr auto cc_networks = std::array{")
    outfile.write(''.join(ccNetworkDefs))
    outfile.write("""
};

const FakerDataCreditCardNetwork *faker_data_cc_networks(size_t *outNumEntries) {
    if (outNumEntries != 0) {
        *outNumEntries = cc_networks.size();
    }
    return cc_networks.data();
}
""")


def write_url_protocols(outfile):
    with(open(os.path.join('internet', 'url_protocols.csv'))) as csvfile:
        write_string_entries(csvfile, outfile, "url_protocols", "PROTOCOL")


def write_top_level_domains(outfile):
    with(open(os.path.join('internet', 'top_level_domains.csv'))) as csvfile:
        write_string_entries(csvfile, outfile, "top_level_domains", "DOMAIN")


def write_aircraft_types(outfile):
    with(open(os.path.join('aviation', 'aircraft_types.csv'))) as csvfile:
        write_string_entries(csvfile, outfile, "aircraft_types", "TYPE")


def write_airports(outfile):
    with(open(os.path.join('aviation', 'airports.csv'))) as csvfile:
        write_aviation_entries(csvfile, outfile, "airports")


def write_airplanes(outfile):
    with(open(os.path.join('aviation', 'airplanes.csv'))) as csvfile:
        write_aviation_entries(csvfile, outfile, "airplanes")


def write_airlines(outfile):
    with(open(os.path.join('aviation', 'airlines.csv'))) as csvfile:
        write_aviation_entries(csvfile, outfile, "airlines")


def write_colors(outfile):
    methodSuffix = 'colors'
    with(open(os.path.join('colors', 'colors.csv'))) as csvfile:
        outfile.write("\nstatic constexpr auto {method} = std::array{{".format(method=methodSuffix))
        reader = csv.DictReader(csvfile)
        for row in reader:
            outfile.write(
                """
    FakerDataColorEntry{{
        .name={name},
        .hex={hex},
        .red={red},
        .green={green},
        .blue={blue},
        .versionInfo={{
            .added={version},
            .removeInfo={{
                .wasRemoved={wasRemoved},
                .removedIn={removed}
            }}
        }}
    }},""".format(
                    name=c_str(row['name']),
                    version=c_version(row[versionColumn]),
                    hex=c_str(row['hex']),
                    red=row['red'],
                    green=row['green'],
                    blue=row['blue'],
                    removed=c_removed_version(row[removedColumn]),
                    wasRemoved=c_was_removed(row[removedColumn])
                )
            )
        outfile.write("""
    }};
    
    const FakerDataColorEntry *faker_data_{method}(size_t *outNumEntries) {{
        if (outNumEntries != 0) {{
            *outNumEntries = {method}.size();
        }}
        return {method}.data();
    }}
    """.format(method=methodSuffix))


def write_mime_types(outfile):
    with(open(os.path.join('computer', 'mime_types.csv'))) as csvfile:
        write_string_entries(csvfile, outfile, "mime_types", valueColumn="TYPE")


def write_file_extensions(outfile):
    with(open(os.path.join('computer', 'file_extensions.csv'))) as csvfile:
        write_string_entries(csvfile, outfile, "file_extensions", valueColumn="EXT")


def write_file_types(outfile):
    with(open(os.path.join('computer', 'file_types.csv'))) as csvfile:
        write_string_entries(csvfile, outfile, "file_types", valueColumn="TYPE")


def write_file_mime_mapping(outfile):
    methodSuffix = 'file_mime_mapping'
    with(open(os.path.join('computer', 'file_mime_mapping.csv'))) as csvfile:
        outfile.write("\nstatic constexpr auto {method} = std::array{{".format(method=methodSuffix))
        reader = csv.DictReader(csvfile)
        for row in reader:
            outfile.write(
                """
    FakerDataFileMimeMappingEntry{{
        .mimeType={mime},
        .extension={ext},
        .versionInfo={{
            .added={version},
            .removeInfo={{
                .wasRemoved={wasRemoved},
                .removedIn={removed}
            }}
        }}
    }},""".format(
                    mime=c_str(row['MIME']),
                    ext=c_str(row['EXTENSION']),
                    version=c_version(row['VERSION']),
                    removed=c_removed_version(row['REMOVED']),
                    wasRemoved=c_was_removed(row['REMOVED'])
                )
            )
        outfile.write("""
    }};
    
    const FakerDataFileMimeMappingEntry *faker_data_{method}(size_t *outNumEntries) {{
        if (outNumEntries != 0) {{
            *outNumEntries = {method}.size();
        }}
        return {method}.data();
    }}
    """.format(method=methodSuffix))


def write_cmake_system_names(outfile):
    with(open(os.path.join('computer', 'cmake_system_names.csv'))) as csvfile:
        write_string_entries(csvfile, outfile, "cmake_system_names", valueColumn="NAME")


def write_cpu_architectures(outfile):
    with(open(os.path.join('computer', 'cpu_architectures.csv'))) as csvfile:
        write_string_entries(csvfile, outfile, "cpu_architectures", valueColumn="ARCH")


def write_os(outfile):
    with(open(os.path.join('computer', 'os.csv'))) as csvfile:
        write_string_entries(csvfile, outfile, "os", valueColumn="OS")


def write_timezones(outfile):
    with(open(os.path.join('world', 'timezones.csv'))) as csvfile:
        write_string_entries(csvfile, outfile, "timezones", valueColumn="NAME")

def write_ascii_char_groups(outfile):
    with(open(os.path.join('text', 'ascii_char_groups.csv'))) as csvfile:
        write_character_groups(
            csvfile,
            outfile,
            'ascii_character_set_groups',
            nameColumn="GROUP",
            valueColumn="CHARACTERS_HEX_RANGE",
            descriptionColumn="DESCRIPTION"
        )

def write_eascii_char_groups(outfile):
    with(open(os.path.join('text', 'eascii_char_groups.csv'))) as csvfile:
        write_character_groups(
            csvfile,
            outfile,
            'extended_ascii_character_set_groups',
            nameColumn="GROUP",
            valueColumn="CHARACTERS_HEX_RANGE",
            descriptionColumn="DESCRIPTION"
        )

def write_unicode_char_categories(outfile):
    with(open(os.path.join('text', 'unicode_char_categories.csv'))) as csvfile:
        valueColumn = 'HEX_RANGES'
        categoryColumn = 'CATEGORY'
        versionColumn = 'VERSION'
        removedColumn = 'REMOVED'
        methodSuffix = 'unicode_character_categories'

        reader = csv.DictReader(csvfile)

        rows = []
        for row in reader:
            rows.append(row)
            values = [x.split('..') for x in row[valueColumn].replace(' ', '').split('|')]

            outfile.write("\nstatic constexpr auto {method}_{category} = std::array{{\n".format(
                method=methodSuffix,
                category=row[categoryColumn],
            ))

            for range in values:
                if len(range) == 1:
                    range = [range[0], range[0]]
                outfile.write("\tFakerDataUnicodeRange{{.start=0x{start}, .end=0x{end}}},\n".format(
                    start=range[0],
                    end=range[1]
                ))

            outfile.write("};\n")

        outfile.write("\nstatic constexpr auto {method} = std::array{{".format(method=methodSuffix))

        for row in rows:
            outfile.write(
                """
        FakerDataUnicodeCategory{{
            .numRanges={method}_{category}.size(),
            .ranges={method}_{category}.data(),
            .category=UNICODE_CATEGORY_{groupName},
            .versionInfo={{
                .added={version},
                .removeInfo={{
                    .wasRemoved={wasRemoved},
                    .removedIn={removed}
                }}
            }}
        }},""".format(
                    method=methodSuffix,
                    category=row[categoryColumn],
                    groupName=row[categoryColumn].upper(),
                    version=c_version(row[versionColumn]),
                    removed=c_removed_version(row[removedColumn]),
                    wasRemoved=c_was_removed(row[removedColumn])
                )
            )
        outfile.write("""
    }};
    
    const FakerDataUnicodeCategory *faker_data_{method}(size_t *outNumEntries) {{
        if (outNumEntries != 0) {{
            *outNumEntries = {method}.size();
        }}
        return {method}.data();
    }}
    """.format(method=methodSuffix))


with(open(sys.argv[1], 'w')) as outfile:
    outfile.write("""/*************************************************
    AUTOGENERATED FILE! DO NOT EDIT MANUALLY!
*************************************************/

#include <faker-data/faker-data.h>
#include <array>

int faker_data_is_active(FakerDataVersionNumber desiredVersion, const FakerDataVersionInfo* versionInfo) {
    return versionInfo != NULL && desiredVersion >= versionInfo->added &&
        (!versionInfo->removeInfo.wasRemoved || desiredVersion < versionInfo->removeInfo.removedIn);
}

""")
    write_countries(outfile)
    write_given_names(outfile)
    write_surnames(outfile)
    write_suffixes(outfile)
    write_prefixes(outfile)
    write_edge_cases(outfile)
    write_xss(outfile)
    write_sql_injection(outfile)
    write_format_injection(outfile)
    write_currencies(outfile)
    write_account_names(outfile)
    write_transaction_types(outfile)
    write_cc_networks(outfile)
    write_url_protocols(outfile)
    write_top_level_domains(outfile)
    write_aircraft_types(outfile)
    write_airports(outfile)
    write_airplanes(outfile)
    write_airlines(outfile)
    write_colors(outfile)
    write_mime_types(outfile)
    write_file_extensions(outfile)
    write_file_types(outfile)
    write_file_mime_mapping(outfile)
    write_cmake_system_names(outfile)
    write_cpu_architectures(outfile)
    write_os(outfile)
    write_timezones(outfile)
    write_ascii_char_groups(outfile)
    write_eascii_char_groups(outfile)
    write_unicode_char_categories(outfile)