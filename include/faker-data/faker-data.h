#ifndef FAKER_DATA_FAKER_DATA_H
#define FAKER_DATA_FAKER_DATA_H

#include <stddef.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @defgroup Aviation Aviation
 * @brief Data sets related to aviation (airplanes, airports, etc)
 * @{ @}
 */

/**
 * @defgroup Colors Colors
 * @brief Data sets related to colors
 * @{ @}
 */

/**
 * @defgroup Computers Computers
 * @brief Data sets related to computers
 * @{ @}
 */

/**
 * @defgroup Financial Financial
 * @brief Data sets related to financial details and information
 * @{ @}
 */

/**
 * @defgroup Internet Internet
 * @brief Data sets related to internet and online information
 * @{ @}
 */

/**
 * @defgroup Malicious Malicious
 * @brief Data sets related to malicious attacks and edge cases which can cause programs to misbehave and/or corrupt data
 * @{ @}
 */

/**
 * @defgroup People People
 * @brief Data sets related to people (e.g. names)
 * @{ @}
 */

/**
 * @defgroup Utils Utilities
 * @brief Utility methods that can be used for any group
 * @{ @}
 */

/**
 * @defgroup World World
 * @brief Data sets related to the world
 * @{ @}
 */

/**
 * @defgroup Text Text
 * @brief Data sets related to text
 * @{ @}
 */

/**
 * @defgroup DataStructures Data Structures
 * @brief Data structures used for return types
 * @{ @}
 */

/**
 * @enum FakerDataVersionNumber
 * @ingroup DataStructures
 * Enum representing the data versions which are available
 */
typedef enum FakerDataVersionNumber : size_t {
    FAKER_DATA_VERSION_0,

    LATEST = FAKER_DATA_VERSION_0
} FakerDataVersionNumber;

/**
 * @struct FakerDataRemovedVersion
 * @ingroup DataStructures
 * Struct representing if a record was removed in a specific version
 * Do NOT check the `removedVersion` field if the `removed` field is 0!
 *
 * A record is considered "removed" iff "removed" is non-zero AND the desired
 *  data version is greater than or equal to the `removedVersion`
 *
 * @see faker_data_is_active to see if an entry is active
 */
typedef struct FakerDataRemovedVersion {
    /**
     * Whether a record has been removed
     */
    int wasRemoved;

    /**
     * Which version the record was removed in
     */
    FakerDataVersionNumber removedIn;
} FakerDataRemovedVersion;

/**
 * @struct FakerDataVersionInfo
 * @ingroup DataStructures
 * Version information for a record
 */
typedef struct FakerDataVersionInfo {
    /**
     * When a record was added
     */
    FakerDataVersionNumber added;

    /**
     * Information about if and when a record was removed
     */
    FakerDataRemovedVersion removeInfo;
} FakerDataVersionInfo;

/**
 * @enum FakerDataComplexity
 * @ingroup DataStructures
 * Represents the complexity of text-based data
 * Text data is data that's usually entered free-form by users
 * The complexity levels determine how hard it is for most programs
 *  to properly handle the sample data
 */
typedef enum FakerDataComplexity {
    FAKER_DATA_COMPLEXITY_RUDIMENTARY,
    FAKER_DATA_COMPLEXITY_BASIC,
    FAKER_DATA_COMPLEXITY_INTERMEDIATE,
    FAKER_DATA_COMPLEXITY_ADVANCED,
    FAKER_DATA_COMPLEXITY_COMPLEX,
} FakerDataComplexity;

/**
 * @struct FakerDataColorEntry
 * @ingroup DataStructures
 * Represents named colors. Names are based on the valid list of named CSS colors
 */
typedef struct FakerDataColorEntry {
    const char *name;

    const char *hex;

    unsigned char red;
    unsigned char green;
    unsigned char blue;

    /**
     * The version info for the data element
     */
    FakerDataVersionInfo versionInfo;
} FakerDataColorEntry;

/**
 * @struct FakerDataTextEntry
 * @ingroup DataStructures
 * Represents free-form text entries, usually related to fields inputted
 * by humans.
 *
 * These fields have a complexity level for how hard it is for programs
 * to properly handle them (e.g. alphabetic ASCII, printable ASCII, UCS-2,
 * full Unicode)
 *
 * All text that's returned is in UTF-8 encoding. Keep that in mind when
 * converting to the format needed for your language (e.g. UTF-16 for JavaScript)
 */
typedef struct FakerDataTextEntry {
    /**
     * The actual text data, encoded with UTF-8
     */
    const char *text;

    /**
     * The programmatic complexity for handling the specific input
     */
    FakerDataComplexity complexity;

    /**
     * The version info for the data element
     */
    FakerDataVersionInfo versionInfo;
} FakerDataTextEntry;

/**
 * @struct FakerDataStringEntry
 * @ingroup DataStructures
 * Represents string data which is any set of UTF-8 characters
 * Note: This data structure guarantees that strlen is accurate.
 */
typedef struct FakerDataStringEntry {
    /**
     * The UTF-8 string data
     */
    const char *string;

    /**
     * The version info for the data element
     */
    FakerDataVersionInfo versionInfo;
} FakerDataStringEntry;

/**
 * @struct FakerDataAviationEntry
 * @ingroup DataStructures
 * Represents aviation data which
 */
typedef struct FakerDataAviationEntry {
    /**
     * Name of the aviation data entry (UTF-8)
     */
    const char *name;

    /**
     * IATA code for the aviation data
     */
    const char *iata;

    /**
     * ICAO code for the aviation data
     */
    const char *icao;

    /**
     * The version info for the data element
     */
    FakerDataVersionInfo versionInfo;
} FakerDataAviationEntry;

/**
 * @struct FakerDataLengthStringEntry
 * @ingroup DataStructures
 * Represents string data which is any set of UTF-8 characters and is of a given length
 * Note: This data structure does NOT guarantee that strlen is accurate. Instead, use the len property!
 */
typedef struct FakerDataLengthStringEntry {
    /**
     * The length of the UTF-8 data (may be different than what strlen returns)
     */
    size_t len;

    /**
     * The UTF-8 string data
     */
    const char *string;

    /**
     * The version info for the data element
     */
    FakerDataVersionInfo versionInfo;
} FakerDataLengthStringEntry;

/**
 * @struct FakerDataCountry
 * @ingroup DataStructures
 * Country/territory information
 */
typedef struct FakerDataCountry {
    /**
     * The English name of the country
     */
    const char * name;

    /**
     * The U.S. GENC code of the country.
     * May be the empty string if one does not exist
     */
    const char * genc;

    /**
     * The ISO Alpha-2 code of the country.
     * May be the empty string if one does not exist
     */
    const char * isoAlpha2;

    /**
     * The ISO Alpha-3 code of the country.
     * May be the empty string if one does not exist
     */
    const char * isoAlpha3;

    /**
     * The ISO Numeric code of the country.
     * May be the empty string if one does not exist
     */
    const char * isoNumericCode;

    /**
     * The U.S. Stanag code of the country.
     * May be the empty string if one does not exist
     */
    const char * stanag;

    /**
     * The top-level domain of the country.
     * May be the empty string if one does not exist
     */
    const char * domain;

    /**
     * The version info for the data element
     */
    FakerDataVersionInfo versionInfo;
} FakerDataCountry;

/**
 * @struct FakerDataCreditCardNetwork
 * @ingroup DataStructures
 * Information about a credit card network
 */
typedef struct FakerDataCreditCardNetwork {
    /**
     * The text ID of the credit card network
     */
    const char *id;

    /**
     * The human readable name of the credit card network
     */
    const char *name;

    /**
     * The number of different starting sequences for cards issued by the network
     */
    size_t numCardStarts;

    /**
     * A list of starting sequences for cards issued by the network
     */
    const char * const*cardStarts;

    /**
     * The number of different card number lengths issued by the network
     */
    size_t numCardLens;

    /**
     * A list of card number lengths issued by the network
     */
    const size_t *cardLens;

    /**
     * The CVV code length for cards issued by the network
     */
    size_t cvvLen;

    /**
     * The version info for the data element
     */
    FakerDataVersionInfo versionInfo;
} FakerDataCreditCardNetwork;

/**
 * @struct FakerDataCharRange
 * @ingroup DataStructures
 * Represents a range of ASCII (or Extended ASCII) characters. Using unsigned chars to be able to represent extended ascii charsets
 */
typedef struct FakerDataCharRange {
    /**
     * Starting character
     */
    unsigned char start;

    /**
     * Ending character
     */
    unsigned char end;
} FakerDataCharRange;

/**
 * @struct FakerDataCharSetGroup
 * @ingroup DataStructures
 * Represents a named group of ASCII (or Extended ASCII) characters
 */
typedef struct FakerDataCharSetGroup {
    /**
     * Number of ranges in our set
     */
    size_t numRanges;

    /**
     * Array of character ranges
     */
    const FakerDataCharRange * ranges;

    /**
     * Name of the character set group
     */
    const char* name;

    /**
     * Description of the character group
     */
    const char* description;

    /**
     * Version information
     */
    FakerDataVersionInfo versionInfo;
} FakerDataCharSetGroup;

typedef enum FakerDataUnicodeCategoryCode {
    UNICODE_CATEGORY_CC,
    UNICODE_CATEGORY_CF,
    UNICODE_CATEGORY_CO,
    UNICODE_CATEGORY_CS,
    UNICODE_CATEGORY_LL,
    UNICODE_CATEGORY_LM,
    UNICODE_CATEGORY_LO,
    UNICODE_CATEGORY_LT,
    UNICODE_CATEGORY_LU,
    UNICODE_CATEGORY_MC,
    UNICODE_CATEGORY_ME,
    UNICODE_CATEGORY_MN,
    UNICODE_CATEGORY_ND,
    UNICODE_CATEGORY_NL,
    UNICODE_CATEGORY_NO,
    UNICODE_CATEGORY_PC,
    UNICODE_CATEGORY_PD,
    UNICODE_CATEGORY_PE,
    UNICODE_CATEGORY_PF,
    UNICODE_CATEGORY_PI,
    UNICODE_CATEGORY_PO,
    UNICODE_CATEGORY_PS,
    UNICODE_CATEGORY_SC,
    UNICODE_CATEGORY_SK,
    UNICODE_CATEGORY_SM,
    UNICODE_CATEGORY_SO,
    UNICODE_CATEGORY_ZL,
    UNICODE_CATEGORY_ZP,
    UNICODE_CATEGORY_ZS,
} FakerDataUnicodeCategoryCode;

/**
 * @struct FakerDataCharRange
 * @ingroup DataStructures
 * Represents a range of Unicode characters. Using uint32_t for representing unicode runes (full 32-bit values, no encoding)
 */
typedef struct FakerDataUnicodeRange {
    /**
     * Starting character
     */
    uint32_t start;

    /**
     * Ending character
     */
    uint32_t end;
} FakerDataUnicodeRange;

/**
 * @struct FakerDataCharSetGroup
 * @ingroup DataStructures
 * Represents a category of Unicode characters
 */
typedef struct FakerDataUnicodeCategory {
    /**
     * Number of ranges in our set
     */
    size_t numRanges;

    /**
     * Array of character ranges
     */
    const FakerDataUnicodeRange * ranges;

    /**
     * Key of the abbreviated character category (e.g. Cc, Ps, Po, etc.)
     */
    FakerDataUnicodeCategoryCode category;

    /**
     * Version information
     */
    FakerDataVersionInfo versionInfo;
} FakerDataUnicodeCategory;

/**
 * @struct FakerDataCurrency
 * @ingroup DataStructures
 * Information about global currencies
 */
typedef struct FakerDataCurrency {
    /**
     * The English name of the currency
     */
    const char *name;

    /**
     * The ISO code for the currency
     * May be empty if an ISO code is not assigned
     */
    const char *isoCode;

    /**
     * The symbol for the currency
     * May be empty if not assigned
     */
    const char * symbol;

    /**
     * The version info for the data element
     */
    FakerDataVersionInfo versionInfo;
} FakerDataCurrency;

/**
 * @enum FakerDataPersonSex
 * @ingroup DataStructures
 * Represents a person's sex. Used for selections between male and female.
 */
typedef enum FakerDataPersonSex : size_t {
    FAKER_DATA_PERSON_SEX_MALE,
    FAKER_DATA_PERSON_SEX_FEMALE,
} FakerDataPersonSex;

/**
 * @struct FakerDataPersonNamePieceEntry
 * @ingroup DataStructures
 * Represents a piece of a person's name which relies on the person's sex
 */
typedef struct FakerDataPersonNamePieceEntry {
    /**
     * The UTF-8 encoded name
     */
    const char *name;

    /**
     * The complexity level of the name
     */
    FakerDataComplexity complexity;

    /**
     * The sex the name segment is commonly associated with
     */
    FakerDataPersonSex sex;

    /**
     * The version info for the data element
     */
    FakerDataVersionInfo versionInfo;
} FakerDataPersonNamePieceEntry;

/**
 * @struct FakerDataFileMimeMappingEntry
 * @ingroup DataStructures
 * Represents a file to MIME type mapping
 */
typedef struct FakerDataFileMimeMappingEntry {
    const char *mimeType;
    const char *extension;

    /**
     * The version info for the data element
     */
    FakerDataVersionInfo versionInfo;
} FakerDataFileMimeMappingEntry;

/**
 * @ingroup Utils
 * Given a desired version number and an element's version information, returns whether the element
 * was active during for that data version
 * @param desiredVersion Desired version number (i.e. the data version being targetted)
 * @param versionInfo The element's version information (if null, will always return 0)
 * @return
 */
int faker_data_is_active(FakerDataVersionNumber desiredVersion, const FakerDataVersionInfo* versionInfo);

/**
 * @ingroup People
 * Gets the data set of given names
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataPersonNamePieceEntry *faker_data_given_names(size_t *outNumEntries);

/**
 * @ingroup People
 * Gets the data set of surnames
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataTextEntry *faker_data_surnames(size_t *outNumEntries);

/**
 * @ingroup People
 * Gets the data set of name suffixes
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataTextEntry *faker_data_suffixes(size_t *outNumEntries);

/**
 * @ingroup People
 * Gets the data set of name prefixes
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataPersonNamePieceEntry *faker_data_prefixes(size_t *outNumEntries);

/**
 * @ingroup World
 * Gets the data set of countries
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataCountry *faker_data_countries(size_t *outNumEntries);

/**
 * @ingroup World
 * Gets the data set of timezones
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataStringEntry *faker_data_timezones(size_t *outNumEntries);

/**
 * @ingroup Financial
 * Gets the data set of account names
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataTextEntry *faker_data_account_names(size_t *outNumEntries);

/**
 * @ingroup Financial
 * Gets the data set of transaction types
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataStringEntry *faker_data_transaction_types(size_t *outNumEntries);

/**
 * @ingroup Financial
 * Gets the data set of credit card networks
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataCreditCardNetwork *faker_data_cc_networks(size_t *outNumEntries);

/**
 * @ingroup Financial
 * Gets the data set of currencies
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataCurrency *faker_data_currencies(size_t *outNumEntries);

/**
 * @ingroup Malicious
 * Gets the data set of SQL injection attacks
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataStringEntry *faker_data_sql_injection(size_t *outNumEntries);

/**
 * @ingroup Malicious
 * Gets the data set of XSS attacks
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataStringEntry *faker_data_xss(size_t *outNumEntries);

/**
 * @ingroup Malicious
 * Gets the data set of format injection attacks
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataStringEntry *faker_data_format_injection(size_t *outNumEntries);

/**
 * @ingroup Malicious
 * Gets the data set of edge cases (e.g. bad encodings, empty string, long strings, etc)
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataLengthStringEntry *faker_data_edge_cases(size_t *outNumEntries);

/**
 * @ingroup Internet
 * Gets the data set of url protocols (e.g. http, ftp, etc)
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataStringEntry *faker_data_url_protocols(size_t *outNumEntries);

/**
 * @ingroup Internet
 * Gets the data set of top level domains (e.g. "com", "net", "gov")
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataStringEntry *faker_data_top_level_domains(size_t *outNumEntries);

/**
 * @ingroup Aviation
 * Gets the data set of aircraft types
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataStringEntry *faker_data_aircraft_types(size_t *outNumEntries);

/**
 * @ingroup Aviation
 * Gets the data set of airlines
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataAviationEntry *faker_data_airlines(size_t *outNumEntries);

/**
 * @ingroup Aviation
 * Gets the data set of airplanes
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataAviationEntry *faker_data_airplanes(size_t *outNumEntries);

/**
 * @ingroup Aviation
 * Gets the data set of airports
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataAviationEntry *faker_data_airports(size_t *outNumEntries);

/**
 * @ingroup Colors
 * Gets the data set of colors
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataColorEntry *faker_data_colors(size_t *outNumEntries);

/**
 * @ingroup Computers
 * Gets the data set of mime types
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataStringEntry *faker_data_mime_types(size_t *outNumEntries);

/**
 * @ingroup Computers
 * Gets the data set of file extensions
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataStringEntry *faker_data_file_extensions(size_t *outNumEntries);

/**
 * @ingroup Computers
 * Gets the data set of file types
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataStringEntry *faker_data_file_types(size_t *outNumEntries);

/**
 * @ingroup Computers
 * Gets the data set of file MIME mappings
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataFileMimeMappingEntry *faker_data_file_mime_mapping(size_t *outNumEntries);

/**
 * @ingroup Computers
 * Gets the data set of CMAKE system names
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataStringEntry *faker_data_cmake_system_names(size_t *outNumEntries);

/**
 * @ingroup Computers
 * Gets the data set of CPU architectures
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataStringEntry *faker_data_cpu_architectures(size_t *outNumEntries);

/**
 * @ingroup Computers
 * Gets the data set of Operating System names
 * @param outNumEntries The out parameter for the size of the data set
 * @return
 */
const FakerDataStringEntry *faker_data_os(size_t *outNumEntries);

/**
 * @ingroup Text
 * ASCII Character set groups
 * @param outNumEntries  The out parameter for the size of the data set
 * @return
 */
const FakerDataCharSetGroup* faker_data_ascii_character_set_groups(size_t *outNumEntries);

/**
 * @ingroup Text
 * Extended ASCII Character set groups
 * @param outNumEntries  The out parameter for the size of the data set
 * @return
 */
const FakerDataCharSetGroup* faker_data_extended_ascii_character_set_groups(size_t *outNumEntries);

/**
 * @ingroup Text
 * Unicode Character categories and the character ranges available
 * @param outNumEntries  The out parameter for the size of the data set
 * @return
 */
const FakerDataUnicodeCategory * faker_data_unicode_character_categories(size_t *outNumEntries);

#ifdef __cplusplus
}
#endif

#endif //FAKER_DATA_FAKER_DATA_H
